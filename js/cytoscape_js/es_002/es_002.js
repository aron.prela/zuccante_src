window.onload = function() {

  let cy = cytoscape({

  container: document.getElementById('cy'), // container to render in
  // container: $('#cy'),

  elements: [ 
    { 
      data: { id: 'a' }
    },
    { 
      data: { id: 'b' }
    },
    { // edge ab
      data: { id: 'ab', source: 'a', target: 'b' }
    }
  ],

  style: [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)'
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
      }
    }
  ],

  layout: {
    name: 'grid',
    rows: 1
  }
  });

  document.getElementById("btn1").addEventListener("click", function(){
    cy.elements('node#a').style({'background-color': '#f00'});
  });
  document.getElementById("btn2").addEventListener("click", function(){
    cy.elements('edge#ab').style({'line-color': '#f00'});
  });
}