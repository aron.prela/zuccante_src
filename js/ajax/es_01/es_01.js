window.onload = function() {
  let url = "https://api.unsplash.com/search/photos?client_id=f3568b550d2303741855fc9d4a8ece4bac7e4b23e41e5fa61a55f891eca1a378&query=fish";
  let request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.send();
  // log
  console.log("onload")
  console.log(request.status);
  console.log(request.readyState);
    

  document.getElementById("btn").addEventListener("click", function(){
    // log
    console.log("push button");
    console.log(request.status);
    console.log(request.readyState);
    if(request.readyState === 4 && request.status === 200){
      let response = JSON.parse(request.responseText);
      let img = document.getElementById('img');
      img.src = response.results[0].urls.full;
    };
  });
}
