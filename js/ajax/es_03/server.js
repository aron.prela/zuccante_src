/*
curl 127.0.0.1:3000
curl -X POST -d '{"foo" : "bar"}' 127.0.0.1:3000
*/

const http = require('http') 

const port = 3000
const host = '127.0.0.1'

const server = http.createServer((req, res) => {
  console.dir(req.param)

  if (req.method == 'POST') {
    console.log('POST')
    let body = ''
    req.on('data', (chunk) => {
      body += chunk
      console.log('Partial body: ' + body)
    })
    req.on('end', function() {
      data = JSON.parse(body);
      console.log("***************");
      console.log("name: " + data.name);
      console.log("kind: " + data.kind);
      console.log("shiny: " + data.shiny);
      console.log("***************");
      res.writeHead(200, {
        'Content-Type': 'application/json'
      })
      res.end(`{"after" : "POST  ", object: ${body}}`)
    })
  } else {
    console.log('GET')
    res.writeHead(200, {
      'Content-Type': 'application/json'
    })
    res.end(JSON.stringify({method : "GET"}))
  }
})

server.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
