# es04

Approfittiamo per vedere com `app.js` abbia accesso al DOM. L'esempio è tratto da MDN (vedi link qui sotto).

- DOM [qui](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model).
- `Element` [qui](https://developer.mozilla.org/en-US/docs/Web/API/element).
- `HTMLCanvasElement` per un'introduzione [qui](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), per le API [qui](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement).