function coolModule() {

    let something = "cool"; 
    let another = [1, 2, 3];

    function doSomething() { 
        console.log(something);
    }
    
    function doAnother() {
        console.log(another.join("!"));
    }
    
    return {
        doSomething: doSomething, 
        doAnother: doAnother
    };
}

var module = coolModule(); // obtain module
module.doSomething(); 
module.doAnother(); 