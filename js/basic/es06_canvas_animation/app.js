let canvas = document.getElementById('mycanvas');
let ctx = canvas.getContext('2d');

let vel = 3, corner = getRandomInt(20, 120), rad = 20; // velocity, corner, ball radius
        
let ball = { x: 200, y: 150 };

let moveX = Math.cos(Math.PI / 180 * corner) * vel;
let moveY = Math.sin(Math.PI / 180 * corner) * vel;

function loop() {
    
    ctx.clearRect(0, 0, 400, 300);

    if (ball.x > canvas.width - rad || ball.x < rad) moveX = -moveX;
    if (ball.y > canvas.height - rad || ball.y < rad) moveY = -moveY;

    ball.x += moveX;
    ball.y += moveY;

    ctx.beginPath();
    ctx.fillStyle = 'rgb(255, 100, 100)';
    ctx.arc(ball.x, ball.y, rad, 0, Math.PI*2);
    ctx.fill();
    ctx.closePath();
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function restart() {
    vel = 3, corner = getRandomInt(20, 120), rad = 20;
    ball = { x: 200, y: 150 };
}
        
setInterval(loop, 10);