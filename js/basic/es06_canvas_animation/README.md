# es06_canvas_animation

- `beginPath()` [qui](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/beginPath).
- `setInterval(...,...)` [qui](https://developer.mozilla.org/en-US/docs/Web/API/setInterval) per creare l'animazione: il *delay* è in millisecondi.