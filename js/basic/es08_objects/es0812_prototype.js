function doSomething(){}
/* the same as
class do Something{
    constructor(){}
}
*/
console.log( doSomething.prototype );

// an arrow faction is not the same as an usual function
const doSomethingFromArrowFunction = () => {};
console.log( doSomethingFromArrowFunction.prototype );

doSomething.prototype.foo = "bar";
console.log( doSomething.prototype );

let doSomeInstancing = new doSomething();
doSomeInstancing.prop = "some value"; 
console.log( doSomeInstancing );
console.log( doSomeInstancing.__proto__ );
