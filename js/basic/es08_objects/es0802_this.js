function testGlobalThis() {
    console.log(`${this.property}: exists as property`);
}

// in browser the global object is window
globalThis.property = "goal";
testGlobalThis();