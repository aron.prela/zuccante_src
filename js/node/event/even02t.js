const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
let m = 0;
myEmitter.on('event1', () => {
  console.log('event1 :' + (++m));
});
myEmitter.emit('event1');
// Prints: 1
myEmitter.emit('event1');
// Prints: 2
let n = 0;
myEmitter.once('event2', () => {
  console.log('event2 :' + (++n));
});
myEmitter.emit('event2');
// Prints: 1
myEmitter.emit('event2');
// Ignored