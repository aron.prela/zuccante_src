/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('products', {
    productCode: {
      type: DataTypes.STRING(15),
      allowNull: false,
      primaryKey: true
    },
    productName: {
      type: DataTypes.STRING(70),
      allowNull: false
    },
    productLine: {
      type: DataTypes.STRING(50),
      allowNull: false,
      references: {
        model: 'productlines',
        key: 'productLine'
      }
    },
    productScale: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    productVendor: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    productDescription: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    quantityInStock: {
      type: DataTypes.INTEGER(6),
      allowNull: false
    },
    buyPrice: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    MSRP: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  }, {
    tableName: 'products'
  });
};
