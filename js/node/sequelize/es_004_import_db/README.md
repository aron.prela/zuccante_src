# es_004_import_db

A partire dal noto database di test *classicmodels* che può essere trovato ad esempio [qui](http://www.mysqltutorial.org/mysql-sample-database.aspx), e il suo [schema](http://www.mysqltutorial.org/wp-content/uploads/2018/04/MySQL-Sample-Database-Diagram-PDF-A4.pdf). Il database si installa al solito modo
```
mysql < classicmodels.sql
```
e quindi garantendo i privilegi di accesso sempre al solito modo
```
GRANT ALL ON classicmodels.* TO 'classicmodels'@'%' IDENTIFIED BY 'classicmodels';
```
fatto questo preparaziome l'automatismo per la creazione dei *model* (senza doverlo fare a mano), a tal rpoposito vis ono varie soluzioni, ma a noi va benissmo [sequelize-auto](https://github.com/sequelize/sequelize-auto) che noi installeremo localmente, non globalmente come suggerito, assieme al vecchio ma funzionante driver `mysql` (non `mysql2`),
 che tanto ci servirà solo a questo punto
``` javascript
npm install sequelize-auto
npm install mysql
```
Diamo
```
./node_modules/sequelize-auto/bin/sequelize-auto -d classicmodels -h localhost -u classicmodels -x classicmodels -e mysql
```
a questo punto abbiamo ancora le seguenti dipendenze
```
"dependencies": {
    "mariadb": "^2.1.4",
    "mysql": "^2.17.1",
    "sequelize": "^5.21.3",
    "sequelize-auto": "^0.4.29"
  }
```
Con `npm uninstall mysql sequelize-auto`
```
"dependencies": {
    "mariadb": "^2.1.4",
    "sequelize": "^5.21.3"
  }
```
Ritocchiamo a mano i *models* in modo che abbiano nome singolare se lo desideriamo. Abbiamo aggiunto a mano, cosa non necessaria per le *raw query*
``` javascript
Customer.hasMany(Order);
Order.belongsTo(Customer);
```
Le useremo per i join.

## raw query

Questa è la soluzione ideale per query complesse o in cui si voglia far apparire SQL. Il riferimento per la documentazione è [qui](https://sequelize.org/v5/manual/raw-queries.html), ed il lavoro risulta pure agile! Qui sotto il primo esempio
``` javascript
sequelize.query('SELECT customerName, phone FROM customers LIMIT 10').then(([results, metadata]) => {
        results.forEach((result) => console.log(JSON.stringify(result))); 
    });
```
l'oggetto `results`, è una query `SELECT`, contiene i risultati, cioè le righe. Lo stesso risultato si può ottenere mappando sul model `customers` (corrispondente alla tabella `customers`).
``` javascript
sequelize.query('SELECT * FROM customers LIMIT 10', {
        model: Customer
    }).then(customers => {
        customers.forEach((customer) => {
            console.log(`name: ${customer.customerName}`);
            console.log(`phone: ${customer.phone}`);
        }); 
    })
```
la soluzione ideale per quando si lavora su di un singolo model. Se invece volessimo realizzare col nostro ORM un join
``` javascript
Customer.findAll({
        attributes: ['customerName', 'phone', 'city'],
        limit: 10,
        include: [{
            model: Order,
            attributes: ['orderNumber', 'orderDate', 'status'],
            where: { status: 'Shipped'}
        }]
    }).then(customers => {
        customers.forEach((customer) => {
            ...
            customer.ordersforEach((order) => {
               ...
            });
        }); 
    });
```
dobbiamo fare attenzione 
- selezionare i campi e non usare `findAll()` in quanto contiene anche i *time stamps*.
- impostare correttamente le chiavi esterne (qui facciamo uso di un DB preesistente): `Customer.hasMany(Order, {foreignKey: 'customerNumber'});`.