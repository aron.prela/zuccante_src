const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();
const views = require('koa-views');

app
  // .use(body())
  .use(views(__dirname + '/views', {extension: 'ejs'}))
  .use(router.routes());

const drinks = [
    {name: 'spritz al bitter', ingredients: 'bitter e prosecco'},
    {name: 'spritz al select', ingredients: 'bitter e select'},
    {name: 'negroni', ingredients: 'gin, bitter e martini rosso'},
    {name: 'martini dry', ingredients: 'gin, vermouth dry, scorza di limone, ghiaccio e oliva'},
];

router.get('/', async (ctx) => {
    title = 'Bar Jolly';
    header = 'Drink';
    // await ctx.render('index.ejs',{title})
    await ctx.render('index.ejs',{title: title, header: header, drinks, drinks})
});

  
app.listen(3000);