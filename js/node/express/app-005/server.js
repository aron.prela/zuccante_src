const express = require('express');
const cors = require('cors');
const app = express();

/*

SEE cors documentation

app.use(cors());
i.e.
Access-Control-Allow-Origin: *


app.use(cors({
  origin: 'http://localhost:8080'
}));
*/

// set the CORS options


/*
let whitelist = ['http://localhost:8080', 'http://myapp.com']
let corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  methods: ['POST'],
  allowHeaders: ['Content-Type']
}
*/

app.use(express.json()); // to parse json (try to comment)
app.options('/', cors()) // enable pre-flight request for POST request

/*
app.options('*', cors()) // enable pre-flight request for POST request for all


app.post('/',  cors(), (req, res) => { // no preflight o preflisght seguendo la documentazione
  console.log(req.body);
})
*/

app.post('/', (req, res) => {
  console.log(req.body);
})


app.listen(3000, function () {
  console.log('CORS-enabled web server listening on port 3000')
})

