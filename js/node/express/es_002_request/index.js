var express = require("express");
var app = express();

// built-in middleware, it parses incoming requests with JSON payloads (see doc)
app.use(express.json());

app.get('/', (req, res) => {  
    res.json({ msg : 'GET request, /'});
});

app.post('/', (req, res) => {
    res.json({ msg : 'POST request, /'});
});

app.put('/', (req, res) => {
    res.json({ msg : 'PUT request, / '});
});

app.delete("/", (req, res) => {
    res.json({ msg : 'DELETE request, /'});
});

// post json
app.post('/form', (req, res) => {
    const data = req.body.data;
    // send a response as a feedback
    res.json({ post: data });
});

app.listen(3000, () => {
    console.log("App is listening on port 3000");
});