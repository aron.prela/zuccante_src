var promise1 = new Promise(function(resolve, reject) {
  resolve('Success!');
});

promise1.then(function(value) {
  console.log(value);
});


let promise2 = new Promise(function(resolve, reject) { 
  reject('Ahia!');
  // throw 'Ahia!'
});

promise2.catch(function(error) {
  console.log(error);
});