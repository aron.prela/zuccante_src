# es001

Qui facciamo il nostro primo esempio

- `data()` in **Vue.js 3** è una funzione (non più un oggetto) che ritorna l'oggetto *data*, per le API [qui](https://v3.vuejs.org/api/options-data.html#data-2)
```js
// direct instance creation
const data = { a: 1 }

// The object is added to a component instance
const vm = createApp({
  data() {
    return data
  }
}).mount('#app')

console.log(vm.a) // => 1
```
- `methods`: di tipo `{ [key: string]: Function }`, per le API [qui](https://v3.vuejs.org/api/options-data.html#methods).
```js
const app = createApp({
  data() {
    return { a: 1 }
  },
  methods: {
    plus() {
      this.a++
    }
  }
})

const vm = app.mount('#app')

vm.plus()
console.log(vm.a) // => 2
```
- `mounted()`: chiamato quando *instance* è `mounted`, API [qui](https://v3.vuejs.org/api/options-lifecycle-hooks.html#mounted).
- `Vue.createApp()`: un *factory method* che ritorna un *instance*, per le API [qui](https://v3.vuejs.org/api/global-api.html#createapp).
- `mount(...)`: per la QPI [qui](https://v3.vuejs.org/api/application-api.html#mount).