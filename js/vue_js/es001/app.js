const Counter = {
    data() {
        return {
            message: 'You loaded this page on ' + new Date().toLocaleString(),
            counter: 0
        }
    },
    mounted() {
        setInterval(() => {
            this.counter++
        }, 1000)
    },
    methods: {
        reset() {
          this.counter = 0
        }
    }
}
  
Vue.createApp(Counter).mount('#counter')
  