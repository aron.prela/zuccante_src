# es005a

Qui vediamo il primo esempio di componente; [qui](https://v3.vuejs.org/guide/component-basics.html#passing-data-to-child-components-with-props) la guida ufficiale. Questo chiaramente è un esercizio preparatorio
Ecco come usare il nostro componente
```html
<memo text="vla bla bla"></memo>
```
in `v-for` dobbiamo fare attenzione
```html
<memo v-nind:text="item.text"></memo>
```
che da noi appare in forma contratta `:text`.