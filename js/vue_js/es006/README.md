# es006

Il primo esempio con Axios lo abbiamo preso da [qui](https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html), un tutorial obsoleto (ver 2.*) di Vue.js ed aggiornato alla nuova versione di Vue. Per quanto riguarda Axios la sua home page è [qui](https://axios-http.com/).

## la chiamata di axios

Come risposta del server web REST axios prevede il seguente schema
```js
{
  // `data` is the response that was provided by the server
  data: {},

  // `status` is the HTTP status code from the server response
  status: 200,

  // `statusText` is the HTTP status message from the server response
  statusText: 'OK',

  // `headers` the HTTP headers that the server responded with
  // All header names are lower cased and can be accessed using the bracket notation.
  // Example: `response.headers['content-type']`
  headers: {},

  // `config` is the config that was provided to `axios` for the request
  config: {},

  // `request` is the request that generated this response
  // It is the last ClientRequest instance in node.js (in redirects)
  // and an XMLHttpRequest instance in the browser
  request: {}
}
```
nel nostro caso `data.bpi` ritorna
```
{
"USD":{"code":"USD","symbol":"&#36;","rate":"54,141.9218","description":"United States Dollar","rate_float":54141.9218},
"GBP":{"code":"GBP","symbol":"&pound;","rate":"39,746.2345","description":"British Pound Sterling","rate_float":39746.2345},
"EUR":{"code":"EUR","symbol":"&euro;","rate":"46,854.5274","description":"Euro","rate_float":46854.5274 }
}
```
ecco la richiesta in axios
```js
axios
    .get('https://api.coindesk.com/v1/bpi/currentprice.json')
    .then(response => {
        this.info = response.data.bpi
    })
    .catch(error => {
        console.log(error)
        this.errored = true
    })
    .finally(() => this.loading = false)
```
che si spiega da sola!

## filter nel vecchio esempio

Per il dettaglio [qui](https://vuejs.org/v2/guide/filters.html) nella documentazione ufficiale. Lo usiamo nel *mustache* nel modo seguente col pipe
```
{{ currency.rate_float | currencydecimal }}
```
dopo averlo definito
```js
filters: {
    currencydecimal(value) {
        return value.toFixed(2)
    }
},
```
In Vue 3.0 i *filter* vengono abbandonati, utili le *computed property* o metodi come abbiamo fatto noi.

## mounted

Nel ciclo di vita di una app Vue js abbiamo un *Instance Lifecycle Hooks*, [qui](https://vuejs.org/v2/guide/instance.html#Instance-Lifecycle-Hooks) per i dettagli; `mounted()` viene chiamato appunto dopo che l'*instance* è montata
```js
Vue.createApp(Es006).mount('#es006');
```
