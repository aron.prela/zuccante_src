const Es006 = {
    data() {
        return {
            info: null,
            loading: true,
            errored: false
        }
    },
    methods: {
        currencydecimal(value) {
            return value.toFixed(2)
        }
    },
    mounted() {
        axios
            .get('https://api.coindesk.com/v1/bpi/currentprice.json')
            .then(response => {
                this.info = response.data.bpi
            })
            .catch(error => {
                console.log(error)
                this.errored = true
            })
            .finally(() => this.loading = false)
    }
}

Vue.createApp(Es006).mount('#es006');
