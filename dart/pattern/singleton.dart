class Me {
  // the object is created when the class is loaded
  static Me? _singleton;
  static final String _name = "Andrea";

  // facory method overrides default constructor
  factory Me() {
    return _singleton ??= Me._internal();
  }

  // a getter
  static String get name => _name;

  @override
  String toString() => "Hello, my name is $name.";

  // a named constructor
  Me._internal();
}

void main() {
  Me andrea = Me();
  Me anotherAndrea = Me();

  print(andrea);

  String samenessCheck = identical(andrea, anotherAndrea)
      ? "We are both the same ${Me.name}."
      : "We are NOT the same. I mean, just look at us.";
  print(samenessCheck);
}
