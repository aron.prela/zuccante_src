import 'dart:async';

void main() async {
  // a generic stream StreamController<T>
  // final StreamController<dynamic>
  final StreamController ctrl = StreamController();

  final Stream stream = ctrl.stream;
  // final StreamSubscription subscription = stream.listen((data) => print('$data'));
  stream.listen((data) => print('$data'));
  // print("ciao");

  ctrl.sink.add('my name');
  // try to remove await ... await is better
  await Future.delayed(Duration(seconds: 2), () => ctrl.sink.add(1234));
  await Future.delayed(Duration(seconds: 2),
      () => ctrl.sink.add({'a': 'element A', 'b': 'element B'}));
  await Future.delayed(Duration(seconds: 2), () => ctrl.sink.add(123.45));
  ctrl.close();
}
