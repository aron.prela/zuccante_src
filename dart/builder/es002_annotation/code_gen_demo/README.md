# code_gen_demo

La novità rispetto all'esempio precedente è l'uso del pacchetto `source_gen`, [qui](https://pub.dev/packages/source_gen) per la documentazione, sulle FAQ vediamo le differenze col pacchetto `build`. In questo esempio lavoriamo con un singolo pacchetto, questo, per poi utilizzarlo su di un esmpio, `example` e, finalmente, introduciamo all'uso delle *annotation*. Prepariamo la classe **PODO** per le nsotre annotation
``` dart
class MyAnnotation {
  final String prop;
  const MyAnnotation({this.prop});
}
```
sempre analogamente all'esempiom precedente prepariamo il *builder* che qui si chiama *generator*
``` dart
class AnnotationGenerator extends GeneratorForAnnotation<MyAnnotation> {
  @override
  generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) {
    String className = element.displayName;
    String path = buildStep.inputId.path;
    String prop = annotation.peek('prop').stringValue;
    return '''
    // ${DateTime.now()}
    // =====================

    // classname: $className
    // path: $path
    // prop: $prop

    print('Hello MyAnnotation');
    '''
    ;
  } 
}
```
`GeneratorForAnnotation` estende `Generator` pensato appunto per generare codice Dart basandosi su di una libreria. Vediamo quindi di analizzare il metodo
``` dart
@override
generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) 
```
> The element model describes the semantic (as opposed to syntactic) structure of Dart code. ..Generally speaking, an element represents something that is declared in the
code, such as a class, method, or variable.

Ecco che troviamo `ClassElemenet`, `ClassMemberElement`, `ConstructorElement` ed infine `Element`. `BuildSTep` già lo conosciamo e rappresemta un passo nella costruzione. `CostantReader` permette di insagare valori ed altro .. `peek` ci permette di recuperare un oggetto di tipo `CostantReader` il quale ci permette di recuperare il tipo ed il vallre, nel nostro caso come stringa visto che poi dobbiamo scriverla.
