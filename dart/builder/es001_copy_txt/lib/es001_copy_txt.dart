library es001_copy_txt;

import 'package:build/build.dart';
import 'package:es001_copy_txt/src/copy_builder.dart';

Builder copyBuilder(BuilderOptions options) => CopyBuilder();
