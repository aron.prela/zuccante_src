main() {
  Future.delayed(const Duration(seconds: 0), () {
    print("from callback");
    return 666;
  }).then((value) {
    print(value);
  });
  print("from main()");
}
