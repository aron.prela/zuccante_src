import 'dart:math';

class Employee {
  int id;
  String firstName;
  String lastName;

  Employee(this.id, this.firstName, this.lastName);
}

void main() async {
  print("getting employee...");
  var x = await getEmployee(33);
  print("Got back ${x.firstName} ${x.lastName} with id# ${x.id}");
}

Future<Employee> getEmployee(int id) async {
  // try to remove async
  Random rnd = Random();
  int s = 1 + rnd.nextInt(4);
  await Future<void>.delayed(Duration(seconds: s)); // a delay
  Employee e = Employee(id, "Joe", "Coder");
  return e;
}
