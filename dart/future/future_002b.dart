import 'dart:math';

class Employee {
  int id;
  String firstName;
  String lastName;

  Employee(this.id, this.firstName, this.lastName);
}

void main() async {
  print("getting employee...");
  var x = await getEmployee(33);
  print("Got back ${x.firstName} ${x.lastName} with id# ${x.id}");
}

Future<Employee> getEmployee(int id) {
  Random rnd = Random();
  int s = 1 + rnd.nextInt(4);
  return Future<Employee>.delayed(
      Duration(seconds: s), () => Employee(id, "Joe", "Coder")); // a delay
}
