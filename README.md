## src per ITIS Zuccante

Qui sono contenuti sorgenti ed esercitazioni proposte durante i miei anni di insegnamento per TPSIT ed Informatica: il materiale è in costante aggiornamento. Qui sotto i materiali primariamente proposti.

Sul [wiki](https://gitlab.com/divino.marchese/zuccante_src/wikis/home) i percorsi didattici ed i materiali per classi e corsi.