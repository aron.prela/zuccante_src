import java.util.concurrent.CompletableFuture;

public class Future06 {

    public static void main(String[] args) throws Exception {

        CompletableFuture<String> cFut01 = CompletableFuture.supplyAsync(() -> "Hello")
            .thenCompose(s -> CompletableFuture.supplyAsync(() -> s + " World"));

        CompletableFuture<String> cFut02 = CompletableFuture.supplyAsync(() -> "Hello")
            .thenCombine(CompletableFuture.supplyAsync(() -> " World"), (s1, s2) -> s1 + s2);

        CompletableFuture cFut03 = CompletableFuture.supplyAsync(() -> "Hello")
            .thenAcceptBoth(CompletableFuture.supplyAsync(() -> " World"),
              (s1, s2) -> System.out.println(s1 + s2));
          
        String result01 = cFut01.get();
        String result02 = cFut02.get();
        cFut03.get();
        
        System.out.println("result01: " + result01 + "\n" + "result02: " + result02);

    }

}
