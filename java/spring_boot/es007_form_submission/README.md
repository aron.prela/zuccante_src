# es007_form_submission

Qui riproponiamo un tutorial ufficiale [qui](https://spring.io/guides/gs/handling-form-submission/)
- **Spring Boot Dev Tools**
- **Spring Web**
- **Mustache**
  Qui il template!
  
## `@RequestParam`

La documentazione [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/RequestParam.html).
  
## `@ModelAttribute`

Per la documentazione [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/ModelAttribute.html).
> Annotation that binds a method parameter or method return value to a named model attribute,

Attenzione se non specificato (`name` o `id` nel form con un *model*) abbiamo ade sempio una `Map<String, String>`.
nel `@Controller` viene chiamato prima di ogni richiesta (`RequestMapping`).

## altri materiali

[1] "Using Spring MVC’s @ModelAttribute Annotation" SU dzONE[qui](https://dzone.com/articles/using-spring-mvc%E2%80%99s).
Ad esempio (non è il nostro caso).  
[2] "Spring MVC and the @ModelAttribute Annotation" su Baeldung [qui](https://www.baeldung.com/spring-mvc-and-the-modelattribute-annotation)
