# es002_restfull

Questo esempio è adattato da un vecchio tutorial: [qui](https://dzone.com/articles/spring-boot-restful-web-service-complete-example) con aggiornamenti e modifiche.

## le classi 

- `Student` è la classe *model* un POJO cui non va aggiunto alcun costruttore!
- `StudentService` è un *singleton*, un *service* si Spring (veedi documentazione sulle *annotation* e DI), lì troviamo tutti i metodi usati nei *controller*
- `Controller` è il controller, DI del *service* attenzione ad usare l'*annotation* `#RestController`! Nel tutorial originale compare ancora una sintassi prolissa ed in parte obsoleta.

## i feedback

A parte i `log` su terminale usiamo come messaggi: `String` nel caso più semplice, `Student` convertito da *Spring Boot* in `json` e, solo nel caso di registrazione,
un oggetto `StudentRegistrationReply`.

## POST

Useremo `curl` per fare i nostri esperimenti, il `POST` ed il `PUT` lo facciamo in `json`, dovremo pertanto associare l'apposito *header* alla richiesta `http`. Registriamo due studenti
```
curl -X POST -H "Content-Type: application/json" \
-d '{"name":"Bepi Sbrisa","age":34, "registrationNumber":"1234"}' \
http://localhost:8080/student/register

curl -X POST -H "Content-Type: application/json" \
-d '{"name":"Toni Bueghin", "age":46, "registrationNumber":"8888"}' \
http://localhost:8080/student/register
```

## GET

Usando un browser o `curl` possiamo dare
```
curl http://127.0.0.1:8080/student/all
```
o
```
curl -X GET http://127.0.0.1:8080/student/8888
```

## DELETE

Per cancellare
```
curl -X DELETE http://localhost:8080/student/delete/8888
```
verificando quindi con
```
curl http://127.0.0.1:8080/student/all
```

## PUT

Per aggiornare
```
curl -X PUT -H "Content-Type: application/json" \
-d '{"name":"Bepi Sbrisa","age":35, "registrationNumber":"1234"}' \
http://localhost:8080/student/update
```
verificando con
```
curl http://127.0.0.1:8080/student/all
```

## reboot del server

Il server viene riavviato in automatico usando i `devtools`, vedi *Gradle* e settando opportunamente *IDEA*, vedi progetto `demo`.


