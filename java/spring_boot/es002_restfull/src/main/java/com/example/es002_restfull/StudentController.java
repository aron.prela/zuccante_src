package com.example.es002_restfull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {
    
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }
    
    // GET

    @GetMapping("/student/all")
    public List<Student> getAllStudents() {
        return studentService.getStudentRecords();
    }

    @GetMapping("/student/{regdNum}")
    public Student retrieveStudentRecord(@PathVariable("regdNum") String regdNum) {
        System.out.printf("Get %s \n", regdNum);
        return studentService.getStudent(regdNum);
    }

    // DELETE

    @DeleteMapping("/student/delete/{regdNum}")
    public String deleteStudentRecord(@PathVariable("regdNum") String regdNum) {
        System.out.println("In deleteStudentRecord");
        return studentService.deleteStudent(regdNum);
    }
    
    // POST

    @PostMapping("/student/register")
    public StudentRegistrationReply registerStudent(@RequestBody Student student) {
        System.out.println("In registerStudent");
        StudentRegistrationReply stdregreply = new StudentRegistrationReply();
        studentService.add(student);
        // We are setting the below value just to reply a message back to the caller
        stdregreply.setName(student.getName());
        stdregreply.setAge(student.getAge());
        stdregreply.setRegistrationNumber(student.getRegistrationNumber());
        stdregreply.setRegistrationStatus("POST Successful");
        return stdregreply;
    }
    
    // PUT

    @PutMapping("/student/update")
    public String updateStudentRecord(@RequestBody Student stdn) {
        System.out.println("In updateStudentRecord");
        return studentService.upDateStudent(stdn);
    }
}
