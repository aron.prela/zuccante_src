package com.example.es004_crud_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es004CrudHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es004CrudHibernateApplication.class, args);
	}

}
