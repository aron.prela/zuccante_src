# Annotation di Spring e Spring Boot

SpringBott fa uso di varie *annotation* che permettono in modo agile di creare l'applicazione.

## `@SpringBootApplication`

La troviamo sul file di **configurazione** 

> Indicates a configuration class that declares one or more `@Bean` methods and also triggers auto-configuration and component scanning. This is a convenience annotation that is equivalent to declaring `@Configuration`, `@EnableAutoConfiguration` and `@ComponentScan`.

[**doc**](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/SpringBootApplication.html)

Quindi possiamo usare `@Configuration` se vogliamo aggiungere altri file di configurazione con *bean*.

## `@Bean`

I *bean+ un tempo configurati in XML in Spring ora vengono definiti con *annotation*. Un *bean* è un *facrtory method* ed è il protagnosta della ***DI***.

[**doc**](https://docs.spring.io/spring-framework/docs/5.3.8/javadoc-api/org/springframework/context/annotation/Bean.html?is-external=true)

Gli troviamo all'interno di un file di configurazione in genere (sempre nei nostri esempi): vedi successiva *annotation*.

## `@Configuration`

Neòòa dpcimentazione troviamo
> Indicates that a class declares one or more `@Bean` methods and may be processed by the Spring container to generate bean definitions and service requests for those beans at runtime ...

```java
@Configuration
 public class AppConfig {

     @Bean
     public MyBean myBean() {
         // instantiate, configure and return bean ...
     }
 }
```

[***doc***](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/Configuration.html).

## `@Component`

Pensato per classi
> Such classes are considered as candidates for auto-detection; vale a dire che da qualche parte un `@ComponentScan` è pronto a riconoscerli. A differenza di un *bean* dati in ocnfigurazione un *component* è una classe e server definire la logica dell'applicazione e non solo ... ma noi interessano particolari ``

[**doc**](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Component.html)

## `@Service`

Un *component* deputato alla *business logic*

[**doc**](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Service.html).

## `@Controller`

Siamo nel pattern MVC, ancora un *component* qui troviamo i *request mapping*.

[**doc**](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Controller.html)

## `@Repository`

> Indicates that an annotated class is a "Repository", originally defined by Domain-Driven Design (Evans, 2003) as "a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects". 

Qui troviamo li metodi per l'accesso al DB usati nei *controller*.

[**doc**](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Repository.html).


## `@Autowired`

L'annotazione per la DI, possiamo iniettare *component* e *bean*

> Marks a constructor, field, setter method, or config method as to be autowired by Spring's dependency injection facilities. This is an alternative to the JSR-330 Inject annotation, adding required-vs-optional semantics. 

Applicabile a costruttori, campi, metodi e parametri.

[**doc**](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Autowired.html).

`@Qualifier`

Talvolta dobbiamo togliere l'ambiguità; ecco un esempio (tratto da [qui](https://www.baeldung.com/spring-qualifier-annotation))
```java
@Component("fooFormatter")
public class FooFormatter implements Formatter {
 
    public String format() {
        return "foo";
    }
}

@Component("barFormatter")
public class BarFormatter implements Formatter {
 
    public String format() {
        return "bar";
    }
}

// it doesn't work
@Component
public class FooService {
     
    @Autowired
    private Formatter formatter;
}
```
sistemiamo
```java
public class FooService {
     
    @Autowired
    @Qualifier("fooFormatter")
    private Formatter formatter;
}
```
Nella documentazione troviamo
> This annotation may be used on a field or parameter as a qualifier for candidate beans when autowiring

[***doc***](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Qualifier.html)

## `@Value`

Usate per l'*inject* di valori nel file delle *application propery*, ad esempio avendo
```
server.port=9898
```
potremo usare
```java
@Value("${server.ip}")
private String serverIP;
```
per valori di *default*
```java
@Value("${emp.department:Admin}")
private String empDepartment;
```
Nella documentazione troviamo
> Annotation used at the field or method/constructor parameter level that indicates a default value expression for the annotated element. 

[***doc***](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Value.html)

## un po' di sitografia

[1] "Spring Boot Annotations | Beginners guide" di Raja Anbazhagan [qui](https://springhow.com/spring-boot-annotations/).  
[2] "Spring Bean vs Spring Component" di Dinesh [qui](https://www.javahabit.com/2018/12/30/spring-bean-vs-component/).  
[3] "Service" da JournalDev [qui](https://www.journaldev.com/21435/spring-service-annotation).  
[4] "Spring Bott Annotations" [qui](https://javasterling.com/spring-boot/spring-boot-annotations/#bea).
[4] "Spring Boot Annotations With Examples" [qui](https://javatechonline.com/spring-boot-annotations-with-examples/)