package com.example.es010_rest_again;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es010RestAgainApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es010RestAgainApplication.class, args);
	}

}
