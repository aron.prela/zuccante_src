package com.example.es010_rest_again;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student bepi = new Student(
                    "Bepi",
                    "bepi.fureghin@gmail.com",
                    LocalDate.of(2000, Month.JANUARY, 4)
            );
            Student toni = new Student(
                    "Toni",
                    "toni.bueghin@gmail.com",
                    LocalDate.of(2004, Month.MARCH, 15)
            );
            repository.saveAll(List.of(bepi, toni));
        };
    }
}
