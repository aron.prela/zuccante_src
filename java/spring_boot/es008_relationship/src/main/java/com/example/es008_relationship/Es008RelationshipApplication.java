package com.example.es008_relationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es008RelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es008RelationshipApplication.class, args);
	}

}
