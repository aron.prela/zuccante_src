#am021_authentication_form

Questo esempio riprende il tutorial ufficiale [qui](https://spring.io/guides/gs/securing-web/) con alcune lievi modifiche!

## creazione progetto

- Spring Web
- Spring Boot DevTools
- Spring Security
. Mustache
  
## esempio intermedio

Quanto segue non lo proponiamo in questa esercitazione
```java
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("admin")
                .password(passwordEncoder().encode("zuccante@2021"))
                .roles("ADMIN");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
```
In questo caso l'accesso ad ogni URL viene interdetto aparendo la pagina di login autogenerata. Per il *logout* una richiesta `GET` è sufficiente.
```java
<form action="logout" method="get">
  <input type="submit" value="Sign Out"/>
</form>
```
si verrà portati in una pagina di *logout* autogenerata. Passo ulteriore
```java
...
.authorizeRequests()
        .antMatchers("/").permitAll()
        .anyRequest().authenticated()
...
```
avendo un `/resouce` inibito
- se si tenta l'accesso a `/resource` si viene portati automaticamente in `/login` se non connessi,
- se loggati si potrà accedere!

## la configurazione

Un `UserDetailsService` ci permette di offrire il servizio di autenticazione
```
User -> UserDetails -> UserDetailsService
```
- L'interfaccia `UserDetails`, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/UserDetails.html) per la doc.,
chiarisce il passaggio dallo `User` ai suoi *detail* di configurazione.
- La classe `User`, che implementa `UserDetails` ha una classe `UserBuilder` che ci permette di costruire un *user* denza dover definire noi una classe `User`, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/User.UserBuilder.html) per la doc..
risulta interessante il metodo - toles(String ... roles) che viene posto come un'alternativa ad `authorities(String... aut)` solo col `ROLE_` prefix (messo sulla rispettiva *authority*).

Un modo alternativo per inserire gli utenti
```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .passwordEncoder(new BCryptPasswordEncoder())
        .withUser("admin")
        .password(passwordEncoder().encode("zuccante@2021"))
        .roles("ADMIN")
        .and()
        .withUser("user")
        .password(passwordEncoder().encode("user@2021"))
        .roles("USER");
}

@Bean
public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}
```

## altri materiali

[1] "Introduction to Java Config for Spring Security" da Baeldung [qui](https://www.baeldung.com/java-config-spring-security).   
[2] "Spring Security Form Login" da Baeldung [qui](https://www.baeldung.com/spring-security-login).