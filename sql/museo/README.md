# Esercitazione Museo

L'esercitazione la dividiamo in varie fasi.

## Per preparare il DB

Preparare il DB
```
MariaDB [(none)]> create database Museo;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> GRANT ALL ON Museo.* to 'museo'@'%' IDENTIFIED BY 'museo@2020';
Query OK, 0 rows affected (0.01 sec)

MariaDB [(none)]>
```
A questo punto (da terminale e non nel clent testuale), nella directory in cui si trova `museo.sql`
```
mysql -u museo -p  <password> Museo < museo.sql
```
ovvero
```
sudo mysql Museo < museo.sql
```
p, allinterno del client MySQL
```
mysql> use museo;
mysql> source /<path>/museo.sql;
```
Il *backup* si effettua nel modo seguente
```
sudo mysqldump miodb > miodb.sql
```
ovvero usando un *user* come sopra.

## Aggiunta delle chiavi esterne

Per il vicnolo di integrità referenziale [qui](https://mariadb.com/kb/en/foreign-keys/). Per poter inserire il vincolo di integrità referenziale usiamo `ALTER TABLE` [qui](https://mariadb.com/kb/it/alter-table/)
```
| ADD [CONSTRAINT [symbol]]
        FOREIGN KEY [IF NOT EXISTS] [index_name] (index_col_name,...)
        reference_definition
```

## Le query

Diamo alcuni riferimenti

- `SELECT`: per la sintassi e quelche esempio [qui](https://mariadb.com/kb/en/select/).
- `GROUP BY`: [qui](https://mariadb.com/kb/en/group-by/).
- `JOIN`: invece di usare il `SELECT` CON `AND` la sintasso del `JOIN` rende indubbiamnete più leggibile la query [qui](https://mariadb.com/kb/en/join-syntax/) per un instroduzione. Utile anche la lettura di *identifier names* [qui](https://mariadb.com/kb/en/identifier-names/).
