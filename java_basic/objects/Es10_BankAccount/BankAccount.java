import java.util.*;
import java.text.SimpleDateFormat;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BankAccount {

    private static int ncc = 0;
    private int n;
    private String owner;
    private float balance = 0.0f;
    private List<Transaction> history = new ArrayList<>();

    public BankAccount(String name) {
        n = ++ncc;
        owner = name;
        this.history = history;
        this.balance = balance;
    }

    public float getBalance() {
        return balance;
    }

    public int getNumber() {
        return n;
    }    

    public List<BankAccount.Transaction> getHistory() {
        return history;
    }

    // a factory method
    public Transaction makeTransaction(int type, float money) {
        return new Transaction(type, money);
    }

    public void printHistory() {
        String fileName = "hystory" + n + ".txt";
        File file = new File(fileName);

        String header = "CC n: " + n + " **** name: " + owner;
        String content = header + "\n\n";
        for(Transaction t : this.getHistory()) {
            content += t;
        }
        
        try (FileWriter fop = new FileWriter(file)) {
            if (!file.exists()) {
                file.createNewFile();
            }
            fop.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    class Transaction {

        static final int WITHDRAWAL = -1;
        static final int TRANSFER = 1;

        float money;
        int type;
        Date date;

        Transaction(int type, float money) {
            this.money = money;
            this.type = type;
            this.date = new Date();
            // when we make a transaction we modify a bank account
            BankAccount.this.balance += type*money;
            BankAccount.this.history.add(this); 
        } 

        public String toString(){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String date = sdf.format(this.date); 
            String money = (type*this.money >= 0)? "+" + this.money : "-" + this.money;
            return "date: " + date + " *** " + money + "\n";
        }
    }
}
