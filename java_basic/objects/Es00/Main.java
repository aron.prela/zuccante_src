public class Main {
    
    public static void main(String[] args) {
        
        Cat[] gattile = {
            new Cat("Felix", "red", 5, false),
            new Cat("Silvestro", "black", 5, false),
            new Cat("Lilly", "white", 5, true)
        };
        
        for(Cat cat: gattile)
            System.out.println(cat.catNumber + ": " + cat.name);
        
        System.out.println("We have " + Cat.cats + " Cat objects");    
        System.out.println(gattile[0]);
        
    }    
}
