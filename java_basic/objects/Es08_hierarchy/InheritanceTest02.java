public class InheritanceTest02 {

    public static void main(String[] args) {

        SubClass obj = new SubClass();
        System.out.println(obj.protectedField);

    }

}

class SuperClass {

    protected String protectedField = "inside super";

}

class SubClass extends SuperClass {

    public String field = "inside sub";

}