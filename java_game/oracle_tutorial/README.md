# SwingPaintDemo1

Qui usiamo il coltellino svizzero `SwingUtilities`, il metodo `invokeLater(Runnable doRun)` permette la consegna di un evento all *AWT dispatching thread*, prima dell'esecuzione del *task* attendiamo che la "cosa degli eventi" sia svuotata: il tutto in modo **asincrono**.

# SwingPaintDemo2

Qui invece aggiustiamo le dimensioni del `JFrame` in funzione del contenuto, un `JPanel` opportunamente modificato. `JPanel` è un generico contenitore per *lightweight component*.
``` java
@Override
public Dimension getPreferredSize() { ... }

@Override
public void paintComponent(Graphics g) {
    super.paintComponent(g);       
    ...    
}  
```
il primo dà le dimensioni ed il secondo permette di disegnare, leggere [qui](https://docs.oracle.com/javase/tutorial/uiswing/painting/closer.html) in particolare. `g` è un oggetto di tipo `Graphics` una classe astratta - [qui](https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics.html) per le API ed un'interessante sintesi di come lavora il disegno dell'UI in `awt` - nel caso noi lavoriamo con un oggetto del tipo `Graphics2S` - [qui](https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html) per ulteriori detagli.

# SwingPaintDemo3

Qui gestiamo gli eventi del mouse. Per convenienza e semplicità usiamo `MouseAdapter` in modo da intercettare gli eventi del mouse in modo semplice!

# SwingPaintDemo3

Nulla di nuovo in questa versione se non la creazione della classe `RedSquare` e la sua gestione, il resto è come nell'esempio precedente!